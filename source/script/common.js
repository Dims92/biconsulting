var string = 'Hello'
console.log(`${string} World`);

(function($){

  
  // $(function(){
  //   $("body").click();
  // });

  /* ############################################################################################## */
  /* Маска для ввода номера телефона */
  /* ############################################################################################## */

  $(function(){
    $("input[type='tel']").inputmask({
      mask: "+7 999 999 99 99"
    });
  });

  /* ############################################################################################## */
  /* Верхний фильтр языка */
  /* ############################################################################################## */

  if($('.first-content__switch a').length > 0) {
    $('.first-content__switch a').on('click', function(){
      event.preventDefault();
      $('.first-content__switch a').toggleClass('active');
      $('.first-content__switch-content').toggleClass('active');
      $('.second-content').toggleClass('active');
    });

    $('.first-content__switch-content').on('click', function(){
      event.preventDefault();
      $('.first-content__switch a').toggleClass('active');
      $('.first-content__switch-content').toggleClass('active');
      $('.second-content').toggleClass('active');
    });
  }

  /* ############################################################################################## */
  /* Открытие меню */
  /* ############################################################################################## */

  if($('.js-section-header__menu-opener').length > 0) {
    $('.js-section-header__menu-opener').on('click', function(){
      event.preventDefault();

      $('.js-section-big-menu , header , body').toggleClass('menu-active');
      if(!$('.js-section-big-menu__item--active').hasClass('section-big-menu__item--active')) {
        $('.js-section-big-menu__item--active').addClass('section-big-menu__item--active');
      }
    });

    $('.js-section-big-menu__close').on('click', function(){
      menuClosed();
    });

    $('.js-section-big-menu__list li').on('mouseenter', function(){
      console.log('сработало');
      $('.js-section-big-menu__item--active').removeClass('section-big-menu__item--active');
    });
  }

  function menuClosed() {
    console.log('сработал скрипт');
    $('.js-section-big-menu , header , body').removeClass('menu-active');
    $('.js-section-big-menu__item--active').removeClass('.section-big-menu__item--active');
  }

  /* ############################################################################################## */
  /* Открытие меню авторизации/регистрации */
  /* ############################################################################################## */

  if($('.js-section-header__login-link').length > 0) {
    $('.js-section-header__login-link').on('click', function(){
      event.preventDefault();

      /* открытие первого пункта меню до первого ховера */

      if($('.js-section-big-menu').hasClass('menu-active')) {
        $('.js-section-big-menu , header , body').removeClass('menu-active');
        $('.js-section-big-menu__item--active').removeClass('.section-big-menu__item--active');
      }

      $('.js-section-login-menu , header , body').addClass('login-active');
    });

    $('.js-section-login-menu__close').on('click', function(){
      loginMenuClosed();
    });

    $('.js-section-login-menu__login-link').on('click', function(){
      event.preventDefault();
      $('.js-section-login-menu__login--in').addClass('login-in-no-visible');
      $('.js-section-login-menu__login--recovery').addClass('login-recovery-visible');
    });

    $('.js-section-login-menu__login--recovery .section-login-menu__login-submit').on('click', function(){
      event.preventDefault();
      $('.js-section-login-menu__login--recovery').removeClass('login-recovery-visible').addClass('login-recovery-no-visible');
      $('.section-login-menu__login.section-login-menu__login--new').addClass('login-new-visible');
    });

    $('.js-section-login-menu__toggle-link').on('click', function(){
      $('.js-section-login-menu__toggle-link').toggleClass('section-login-menu__toggle-link--active');
      $('.js-section-login-menu__registration').toggleClass('log-reg--no-active');
      $('.js-section-login-menu__wrap').toggleClass('log-reg--no-active');
    });
  }

  function loginMenuClosed() {
    console.log('сработал скрипт');
    $('.js-section-login-menu , header , body').removeClass('login-active');
    $('.section-login-menu__wrap input').val('').removeAttr('checked');
    $('.js-section-login-menu__login--in').removeClass('login-in-no-visible');
    $('.js-section-login-menu__login--recovery').removeClass('login-recovery-visible');
    $('.js-section-login-menu__login--new').removeClass('login-new-visible');
  }

  /* ############################################################################################## */
  /* Открытие pop-up вопроса */
  /* ############################################################################################## */

  if($('.js-section-main__buy-question-link').length > 0) {
    $('.js-section-main__buy-question-link').on('click', function(){
      event.preventDefault();
      $('.js-section-question-pop-up, header , body, .js-overlay').addClass('qustion-active');
    });

    $('.js-overlay, .js-section-question-pop-up__close').on('click', function(){
      event.preventDefault();
      $('.js-section-question-pop-up , header , body, .js-overlay').removeClass('qustion-active');
      $('.section-question-pop-up__wrap input').val('').removeAttr('checked');
    });
  }


  
  if(!$('.js-section-big-menu__item--active').hasClass('section-big-menu__item--active')) {
    $('.js-section-big-menu__item--active').addClass('section-big-menu__item--active');
  }


}(jQuery));
